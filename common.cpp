﻿/*
    common.cpp inludes physical entities that are common for both files
*/
#include "platf_dep.hpp"
#include "common.hpp"

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

char cr_arr[4][MY_N]{ "name", "extension", "time", "undefined" };

char* get_full(char c) {

    switch (c) {
    case 'n':
        return cr_arr[0];
    case 'e':
        return cr_arr[1];
    case 't':
        return cr_arr[2];
    default:
        return cr_arr[3];
    }
}

char flags[3][5] {
    "-lR", "-lRt", "-lRX"
};

// returns flag for bash ls command depending on shorthand (char)
char* ls_flag(char cr) {
    switch(cr) {
    case 't':
        return flags[1];
    case 'e':
        return flags[2];
    default:
        return flags[0];
    }
}
