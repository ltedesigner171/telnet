#include <filesystem>
namespace fs = std::filesystem;
#include <sstream>
#include <list>
#include <sys/stat.h>
#include <sys/types.h>
#include "platf_dep.hpp"

std::stringstream ss;
unsigned counter;  // counts number of files
/*
 path is global, not an argument of recursive function,
 cause it's bad practice for recursive function to have
 arguments that will be in stack
 */
fs::path path;

//comparator of sorting. Global because is used in recursive function
int (*comp)(const fs::path&, const fs::path&);  //comparator

//Hidden files, also called dot files on Unix operating systems
bool isHidden(const fs::path& p)
{
    fs::path name_path = p.filename();
    std::string name = name_path.string();
    if (name != ".." &&
        name != "." &&
        name[0] == '.')
    {
        return true;
    }

    return false;
}

// comparison, not case sensitive.
bool compare_nocase(const std::string& first, const std::string& second)
{
    unsigned int i = 0;
    while ((i < first.length()) && (i < second.length()))
    {
        if (tolower(first[i]) < tolower(second[i]))
            return true;
        else if (tolower(first[i]) > tolower(second[i]))
            return false;
        ++i;
    }
    return (first.length() < second.length());
}

int comp_by_name(const fs::path& first, const fs::path& second) {
    std::string a = first.filename().string();
    std::string b = second.filename().string();
    std::string c = first.extension().string();
    std::string d = second.extension().string();

    /*
    * For ".gitignore" file extension was empty
        so I wrote this check to fix this bug
    */
    if ('.' == a[0]) {
        a = "";
    }
    if ('.' == b[0]) {
        b = "";
    }

    return compare_nocase(a, b);
}
//comparison two files by extension
int comp_by_ex(const fs::path& first, const fs::path& second) {
    std::string a = first.filename().string();
    std::string b = second.filename().string();
    std::string c = first.extension().string();
    std::string d = second.extension().string();
    if (1 <= c.length())
        c = c.substr(1, c.length() - 1);
    if (1 <= d.length())
        d = d.substr(1, d.length() - 1);

    /*
    * For ".gitignore" file extension was empty
        so I wrote this check to fix this bug
    */
    if ('.' == a[0]) {
        c = a.substr(1, a.length() - 1);
    }
    if ('.' == b[0]) {
        d = b.substr(1, b.length() - 1);
    }

    return compare_nocase(c, d);
}

int comp_by_time(const fs::path& first, const fs::path& second) {
    time_t a = get_time(first.string().c_str());
    time_t b = get_time(second.string().c_str());
    return a < b;
}

//This function was written in order to omit hidden files and directories.
void recursive_traversal() {
    std::list<fs::path> files;
    std::list<fs::path> sub_folders;

    auto it = fs::directory_iterator(path);
    for (fs::directory_entry de : it) {
        fs::path p = de.path();
        if (de.is_directory()) {
            if (!isHidden(p)) {
                sub_folders.push_back(p);
            }
        }
        else {
            files.push_back(p);
            ++counter;
        }
    }

    files.sort(comp); //comparator was defined in folder_content function

    ss << std::setw(25) <<"--File name--" << "\t" << "--LAST WRITE TIME--\n";
    for (fs::path p : files) {
        std::string str_p = fs::absolute(p).string();
        const char* c_str_p = str_p.c_str();
        time_t time = get_time(c_str_p);
        char timebuf[100];
        sprintf(timebuf, "%s", ctime(&time));
        ss << std::setw(25) << p.filename().string() << "\t" << timebuf;
    }
    //go deeper
    for (fs::path p : sub_folders) {
        ss << "Subfolder " << p.string() << ":\n";
        ::path = p;
        recursive_traversal();
    }
}

/*
*	returns multiline text with content of folder and
    subfolders sorted by name, or extension, or time
    depending on second argument
    if type == 'n' --- sorting by name
    if type == 'e' --- sorting by extension
    if type == 't' --- sorting by time
*/
std::string folder_content(fs::path path, char type,
                    unsigned *files_number = nullptr) {
    ss.str(std::string());
    if (!fs::is_directory(path)) {
        return "function folder_content: path is not for directory\n";
    }
    ss << "Folder: ";
    if (path.is_relative()) {
        ss << fs::absolute(path).string();
    }
    else {
        ss << path.string();
        path = fs::relative(path);
    }

    ss << "\nContent of this folder:\n";
    ::path = path; // path is global, not an argument
    // choosing comparator function that will be used
    switch (type) {
    case 'e':
        ::comp = comp_by_ex;
        break;
    case 't':
        ::comp = comp_by_time;
        break;
    default:
        ::comp = comp_by_name;
        break;
    }
    ::counter = 0;
    recursive_traversal();
    if (nullptr != files_number)
        *files_number = counter;
    return ss.str();
}
