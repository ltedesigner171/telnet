// platf_dep.hpp --- platform dependent stuff

#ifdef _WIN32

#define _WIN32_WINNT 0x501
#include <winsock2.h>
#include <ws2tcpip.h>

void win_init();

//Declaring this function explicitly in my code.
const char* inet_ntop(int af, const void* src, char* dst, socklen_t size);

//link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

// closing socket considering OS
#define my_close(sockfd) closesocket(sockfd)

#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>

// closing socket considering OS
#define my_close(sockfd) close(sockfd)

#endif

//returns last modification time for
//file with path from argument
time_t get_time(const char *path);
