#include <stdlib.h>
#include <errno.h>
#include <string>
#include <sys/types.h>
#include <signal.h>
#include <cstring>
#include <filesystem>
namespace fs = std::filesystem;
#include <iostream>
using std::cout;

#ifdef __linux__
#include <sys/wait.h>
#endif

#include "platf_dep.hpp"
#include "file_sys.hpp"
#include "common.hpp"

#define BACKLOG 10	 // how many pending connections queue will hold

void sigchld_handler(int s);

int main(void)
{
#ifdef _WIN32
	win_init();
#endif

	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size;
#ifdef __linux__
	struct sigaction sa;
#endif
	const char yes=1;
	char s[INET6_ADDRSTRLEN];
	char buf[MY_MAX_SIZE];
	int temp;   //temporarily used variable in two places

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;    //for any address family (either IPv4 or IPv6, for example)
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

    // The getaddrinfo() function allocates and initializes
    // a linked list of addrinfo structures
	if ((temp = getaddrinfo(NULL, MY_TELNET_PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(temp));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			my_close(sockfd);
			perror("server: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(servinfo); // all done with this structure

	if (p == NULL)  {
		fprintf(stderr, "server: failed to bind\n");
		exit(1);
	}

	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

#ifdef __linux__
	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}
#endif

	fs::path path(".");
    cout << folder_content(path, 'n');
    cout << "Waiting for requests...\n";
    cout << "If you want to stop it just close your terminal window.\n\n";

	while(1) {  // main accept() loop
		sin_size = sizeof their_addr;

		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr *)&their_addr),
			s, sizeof s);
		cout << "Request on files sorting was received from " << s << "\n";

        if (recv(new_fd, buf, MY_MAX_SIZE-1, 0) == -1) {
            perror("recv");
        } else {
			char& cr = buf[0]; // criterion of sorting
			if ('n' == cr || 'e' == cr || 't' == cr) {
				cout << "There was received request on sorting files by "
					<< get_full(buf[0]) << "\n";
				unsigned files_num; // sorted files number
				cout << folder_content(path, buf[0], &files_num);

				temp = sprintf(buf, "%u", files_num);

				if (send(new_fd, buf, temp, 0) == -1)
					perror("send");
				cout << "Sorting was perfomed! Waiting for other requests...\n\n";
			}
        }
		my_close(new_fd);
	}

#ifdef _WIN32
	WSACleanup();
#endif
	return 0;
}

void sigchld_handler(int s)
{
	(void)s; // quiet unused variable warning

	// waitpid() might overwrite errno, so we save and restore it:
	int saved_errno = errno;
#ifdef __linux__
	while(waitpid(-1, NULL, WNOHANG) > 0);
#endif
	errno = saved_errno;
}
