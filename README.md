# Bohdan Akimenko

# Task description

Client/Server application to sort files on remote HOST.

- client has to sort files in his folder and sub-folders

- server will request sorting by name/type/date

- client has to reply with success or failure

- success replay has to include number of files in his folder and sub-folders.

# Instructions for building

1) provide data exchanging between server and client

2) write user-friendly requests for input and provide user information about happening in both programs (server and client)

3) write code that will print content of folder and subfolders sorting by name/extension/time.

4) write application for Linux, then version for Windows, then sources should be cross-compile.

# Features of sortreq program

1) getting hostname and optionally criterion of sorting from user

2) connecting to specified hostname and sending information about criterion of sorting chooosed

3) displaying received response

# Features of listener (that listens the port and waits for requests) program

1) displaying the content of folder and subfolders

2) receiving requests from sortreq program

3) reprinting content of folder after receiving request

4) answering to request by sending total number of files in folder and subfolders.

# Optional features of listener

1) screen cleaning after new request

2) outputting ONLY file name, extension, time and date.

# Project dependencies

g++ for Linux, Microsoft compiler for Windows, Make

# How to build project on Linux

1) Clone repository

```
git clone https://gitlab.com/networking4/telnet.git
```

2) Run Makefile. Just type make and Enter
```
make
```

# How to build project on Windows

1) Clone repository. It's better to place project on C drive because Developer Command Prompt has strange syntax of changing drive.

```
git clone https://gitlab.com/networking4/telnet.git
```

2) Open Developer Command Prompt for VS.

![Developer Command Prompt for VS](screenshots/dev_prompt.png)

In project winsock is used. Winsock is a Microsoft library. It must be compiled with the Microsoft software compiler. So you need cl.

3) make sure that cl is available.
```
cl
```
![cl](screenshots/cl.png)

4) Run Makefile. Just type make and Enter
```
make
```
5) You can type make clean command to delete temporarily created object files.
```
make clean
```

# How to use it on Linux

1) Run listener that will listen to the port. New window will be opened.
```
./listener
```

![listener](screenshots/listener.png)

2) To request a remote HOST to sort files run sortreq program, specified HOST name (e. g. localhost) and type of sorting sorting. You can use three short flags (-n --- by name, -e --- by extension, -t --- by time).
```
./sortreq localhost -e 
```

![sortreq](screenshots/sortreq.png)

You can also use two long flags to specify the same information: --by name, --by time, --by extension.

![long form of request](screenshots/by.png)

If listener is not running sortreq will get fail.

# How to use it on Windows

1) Run (double click) client.exe.

2) Run (double click) sortreq.exe.
Or you can run sortreq.exe from command prompt with flags as on Linux.

![srceenshot of usage in windows](screenshots/windows.png)

# Executables

If something went wrong or you don't want to compile project you can download executables for Linux [here](https://www.dropbox.com/s/32jd5jbesfoxyw4/realese.zip?dl=0) and for Windows [here](https://www.dropbox.com/s/r9urpg3z18va6i5/windows.zip?dl=0).
